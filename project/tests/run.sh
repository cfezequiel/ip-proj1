#! /bin/sh

PROGRAM=../bin/iptool

# IMAGE TESTS 

# Greyscale threshold 
${PROGRAM} input/microscopy_grey_stack1_Z27.pgm output/greythresh.pgm conf/greythresh.roi
${PROGRAM} input/microscopy_grey_stack1_Z27.pgm output/greythresh2.pgm conf/greythresh2.roi
echo 'Greyscale threshold test done'

# Color threshold 
${PROGRAM} input/microscopy_color_stack1_Z27.ppm output/colorthresh.ppm conf/colorthresh.roi 
${PROGRAM} input/baboon_color.ppm output/colorthresh2.ppm conf/colorthresh2.roi 
${PROGRAM} input/baboon_color.ppm output/colorthresh3.ppm conf/colorthresh3.roi 
${PROGRAM} input/baboon_color.ppm output/colorthresh4.ppm conf/colorthresh4.roi 
echo 'Color threshold test done'

# Adaptive weight threshold 
${PROGRAM} input/microscopy_grey_stack3_Z3000.pgm output/adaptivethresh.pgm conf/adaptivethresh.roi
${PROGRAM} input/microscopy_grey_stack3_Z3000.pgm output/adaptivethresh2.pgm conf/adaptivethresh2.roi
${PROGRAM} input/microscopy_grey_stack3_Z3000.pgm output/adaptivethresh3.pgm conf/adaptivethresh3.roi
${PROGRAM} input/microscopy_grey_stack3_Z3000.pgm output/adaptivethresh4.pgm conf/adaptivethresh4.roi
echo 'Adaptive weight threshold test done'

# 2D smoothing
${PROGRAM} input/usf_msc_noise.pgm output/smoothing2d.pgm conf/smoothing2d.roi
${PROGRAM} input/usf_msc_noise.pgm output/smoothing2d2.pgm conf/smoothing2d2.roi
echo '2D Smoothing done'

# 1D smoothing
${PROGRAM} input/usf_msc_noise.pgm output/smoothing1d.pgm conf/smoothing1d.roi
${PROGRAM} input/usf_msc_noise.pgm output/smoothing1d2.pgm conf/smoothing1d2.roi
echo '1D smoothing done'

# VIDEO TESTS

# Greyscale Thresholding
${PROGRAM} -s input/microscopy_grey_stack3.m1v output/vid_greythresh.m1v conf/vid_grey.roi
echo 'Greyscale thresholding video test done'

# Color Thresholding
${PROGRAM} -s input/ball_short.m1v output/vid_colorthresh.m1v conf/vid_colorthresh.roi
echo 'Color thresholding video test done'

# Adaptive Thresholding
${PROGRAM} -s input/microscopy_grey_stack3.m1v output/vid_adaptivethresh.m1v conf/vid_adaptivethresh.roi
echo 'Adaptive thresholding video test done'

# 2D Smoothing
${PROGRAM} -s input/cat_dog_noise.m1v output/vid_smoothing2d.m1v conf/vid_smoothing2d.roi
echo '2D smoothing video test done'

# 1D Smoothing
${PROGRAM} -s input/cat_dog_noise.m1v output/vid_smoothing1d.m1v conf/vid_smoothing1d.roi
echo '1D smoothing video test done'


