#include "../iptools/core.h"
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <strings.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <fstream>
#include <string>
#include <string.h>
#include <vector>
#include <algorithm>
#if HAS_CYCLE_COUNTER
#include <cycle.h>
#endif
using namespace std;

#define MAXLEN 256

enum ProcessType {Image, Video};

int readFiles(const char *dirName, vector<string > &frames)
{
    DIR *dp;
    struct dirent *dirEntry;
    string filename;

    dp = opendir(dirName);
    if (dp == NULL)
    {
        cout << "Error opening " << dirName << endl;
        return errno;
    }

    while ((dirEntry = readdir(dp)))
    {
        filename.assign(dirEntry->d_name);

        if (!filename.compare(".") || !filename.compare(".."))
            continue;

        // Add path to filename
        filename.insert(0, "/");
        filename.insert(0, dirName);

        frames.push_back(filename);
    }

    closedir(dp);

    // Sort frames
    sort(frames.begin(), frames.end());

    return 0;
}

int processVideo(char *src, char *tgt, ROIList roiList) 
{
    int err;
    char outFrame[MAXLEN];
    char srcDir[MAXLEN];
    char tgtDir[MAXLEN];
    char cmd[MAXLEN];
    int type; // 0 - greyscale; 1 - color
    string ext;
    vector<string> frames;
    vector<string>::iterator it;

    // Check if greyscale or color processing to use
    if (!roiList.roiList[0].filter.compare("colorthresh"))
    {
        ext.assign("ppm");
    }
    else
    {
        ext.assign("pgm");
    }

    // Generate temporary video frame directories
    snprintf(srcDir, MAXLEN, "src_%d", rand());
    snprintf(tgtDir, MAXLEN, "tgt_%d", rand());
    snprintf(cmd, MAXLEN, "mkdir -p %s %s", srcDir, tgtDir);
    err = system(cmd);
    if (err)
    {
        printf("Error (%d): Could not create video frame directory/ies\n", err);
        return err;
    }

    // Extract frames from video into frame directory
    snprintf(cmd, MAXLEN, "ffmpeg -v error -i %s %s/imgsrc%%03d.%s", src,
            srcDir, ext.c_str());
    err = system(cmd);
    if (err)
    {
        printf("Error (%d): Failed to extract video frames.\n", err);
        return err;
    }

    // Read input directory filenames
    err = readFiles(srcDir, frames);
    if (err)
    {
        printf("Error (%d): Could not read source video frames.\n", err);
        return err;
    }

    // For each file in directory
    for (int seq = 0; seq < frames.size(); seq++)
    {
        image tgt;

        // Read source frame
        image src;
        src.read(frames[seq].c_str());

        // Set output frame name
#if 0 // Don't use this anymore. 'ext' derived from filter type
        ext.assign(frames[seq].substr(
                   frames[seq].find_last_of(".") + 1).c_str());
#endif
        snprintf(outFrame, MAXLEN, "%s/tgtimg%03d.%s", tgtDir, seq + 1,
                ext.c_str());

        // For each ROI in ROIList
        for (int i = 0; i < roiList.nROI; i++)
        {
            int ss = roiList.roiList[i].soiStart;
            int se = roiList.roiList[i].soiEnd;

#ifdef DEBUG
            printf("Frame %d, ROI(%d), SOI(%d, %d): ", seq+1, i+1, ss, se); 
#endif
            if (seq + 1 >= ss && seq + 1 <= se)
            {

                // Do image processing
                utility::processImage(src, tgt, roiList.roiList[i]);

#ifdef DEBUG
                printf("Applied filter\n");
#endif
            }
            else
            {
                if (tgt.isEmpty())
                {
                    // Copy source frame
                    tgt.copyImage(src);
                }

#ifdef DEBUG
                printf("\n");
#endif
            }
            tgt.save(outFrame);
        }
    }

    // Write frames to output video 
    snprintf(cmd, MAXLEN, "ffmpeg -y -v quiet -i %s/tgtimg%%03d.%s %s", tgtDir,
            ext.c_str(), tgt);
    cout << cmd << endl;
    err = system(cmd);
    if (err)
    {
        printf("Error (%d): Unable to combine frames into video.\n", err);
        return err;
    }

    // Delete temporary frame data
    snprintf(cmd, MAXLEN, "rm -rf %s %s", srcDir, tgtDir);
    err = system(cmd);
    if (err)
    {
        printf("Error (%d): Could not remove temporary frame directories.\n",
                err);
        return err;
    }

    return 0;
}


int main (int argc, char** argv)
{
    ProcessType p = Image;
    int status = 0;
    int c;

    // Check parameters
    if (argc < 4)
    {
        printf("Syntax: %s [-s] <SRC> <DST> <ROI file>\n", argv[0]);
        return -1;
    }

    // Parse options
    while ((c = getopt(argc, argv, "s")) != -1)
    {
        switch (c)
        {
        case 's': // Process video
            p = Video;
            break;

        default: // Process image
            ; // do nothing
        } 
    }

    // Read ROI file
    ROIList roiList(argv[optind + 2]);

    if (p == Image)
    {
        // Read source image
        image src;
        src.read(argv[optind]);

        // Create target image
        image tgt;

        // Apply image filter for each ROI
        for (int i = 0; i < roiList.nROI; i++)
        {
#if HAS_CYCLE_COUNTER
            ticks t0 = getticks();
#endif
            utility::processImage(src, tgt, roiList.roiList[i]);
#if HAS_CYCLE_COUNTER
            ticks t1 = getticks();
            printf("elapsed time: %f\n", elapsed(t1, t0));
#endif
        }

        // Save modified image
        tgt.save(argv[optind + 1]);
    }
    else if (p == Video)
    {
        status = processVideo(argv[optind], argv[optind + 1], roiList);
    }

    return status;
}

