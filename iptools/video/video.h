#ifndef VIDEO_H
#define VIDEO_H

#include "mpeg.h"
#include "../image/image.h"

class video 
{
	private: 
		FILE *mpeg;
		char *framePixels;
		ImageDesc frameImage;
		int numRows;
		int numColumns;
		image img;

	public: 
		video();
		~video();
		
		void setNumberOfRows(int rows);
		void setNumberOfColumns(int cols);
		
		int getNumberOfRows();
		int getNumberOfColumns();
		void getImageFromFrame(image output);
		
		bool saveFrame (int rows, int cols, char* videoFrame);
		bool openVideo (char* fileName);
		bool getNextFrame();
		bool processNextFrame();
		
};

#endif

