#include "video.h"

video::video()
{
	setNumberOfRows(0);
	setNumberOfColumns(0);
}

/*-----------------------------------------------------------------------**/
video::~video()
{}

/*-----------------------------------------------------------------------**/
void video::setNumberOfRows(int rows)
{
	numRows = rows;
}

/*-----------------------------------------------------------------------**/

void video::setNumberOfColumns(int cols)
{
	numColumns = cols;
}

/*-----------------------------------------------------------------------**/
int video::getNumberOfRows()
{
	return numRows;
}

/*-----------------------------------------------------------------------**/
int video::getNumberOfColumns()
{
	return numColumns;
}

/*-----------------------------------------------------------------------**/
bool video::saveFrame (int rows, int cols, char* videoFrame)
{
	for(int i=0; i<rows; i++)
		for(int j=0; j<cols; j++) 
		{
			img.setPixel(i,j,RED,*(videoFrame+3));
			img.setPixel(i,j,GREEN, *(videoFrame+2));
			img.setPixel(i,j,BLUE, *(videoFrame+1));
			videoFrame = videoFrame+4;
		}
	
	return true;
}

/*-----------------------------------------------------------------------**/
bool video::openVideo (char* fileName)
{
	mpeg = fopen (fileName, "rb");
	SetMPEGOption (MPEG_DITHER, FULL_COLOR_DITHER);
	OpenMPEG (mpeg, &frameImage);
	setNumberOfRows(frameImage.Height);
	setNumberOfColumns(frameImage.Width);
	framePixels = (char *) malloc (frameImage.Height*frameImage.Width);
	img.resize(frameImage.Height,frameImage.Width);
}


/*-----------------------------------------------------------------------**/
bool video::getNextFrame()
{
	return GetMPEGFrame(framePixels);
}

/*-----------------------------------------------------------------------**/
bool video::processNextFrame()
{
	if (getNextFrame())
	{
		return saveFrame(getNumberOfRows(),getNumberOfColumns(),framePixels);
	}
}


/*-----------------------------------------------------------------------**/
void video::getImageFromFrame(image output)
{
	output.copyImage(img);
}



