// ROIList.cpp: implementation of the ROIList class.
//
//////////////////////////////////////////////////////////////////////

#include "roi.h"
#include <sstream>
#include <string>
#include <fstream>
using namespace std;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ROIList::ROIList()
{
	this->roiList = NULL;
	this->nROI = 0;

}

ROIList::~ROIList()
{
    if (this->roiList == NULL)
    {
        this->ReleaseROIList();
    }

}

ROIList::ROIList(char *filename)
{
    this->CreateROIList(filename);
}

bool ROIList::InROI(int px, int py)
{
	int x,y,sx,sy;
	for (int i=0;i<this->nROI;i++)
	{
		x=roiList[i].x;
		y=roiList[i].y;
		sx=roiList[i].sx;
		sy=roiList[i].sy;
		if (px>x && py>y && px<x+sx && py<y+sy)
		{
			return true;
		}
		
	}

	return false;

}

void ROIList::CreateROIList(char* filename)
{
    // Open ROIList file
    ifstream file(filename, ifstream::in);
    if (!file.is_open())
    {
        printf("Error: Unable to open ROIList file -> %s\n", filename);
        return;
    }

    // Read number of regions
	file >> nROI;

    this->roiList = new ROI[nROI];
	for (int i = 0; i < nROI; i++)
	{
        // Read ROIList dimensions
        file >> roiList[i].x >> roiList[i].y >> roiList[i].sx >> roiList[i].sy; 

        // Read SOI start and end
        file >> roiList[i].soiStart >> roiList[i].soiEnd;

        // Read filter name
        file >> roiList[i].filter;

        // Read extra parameters
        int val;
        string line;
        getline(file, line);
        istringstream ss(line);
        while (ss.good())
        {
            ss >> val;
            roiList[i].params.push_back(val);
        }
	}

    file.close();
}

void ROIList::ReleaseROIList()
{
    delete this->roiList;
}
