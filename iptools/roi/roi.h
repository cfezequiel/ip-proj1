// ROI.h: interface for the ROI class.
//
//////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>

#if !defined(AFX_ROI_H__8438A090_F2DD_446B_99A9_8C03BAF40432__INCLUDED_)
#define AFX_ROI_H__8438A090_F2DD_446B_99A9_8C03BAF40432__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class ROI
{
public:
	int x;
	int y;
	int sx;
	int sy;
    int soiStart;
    int soiEnd;
    std::string filter;
    std::vector<int> params;

};

class ROIList
{
public:
	int nROI;
    ROI *roiList;
	void CreateROIList(char* filename);
	void ReleaseROIList();
	bool InROI(int px,int py);
	ROIList();
    ROIList(char *filename);
	virtual ~ROIList();
};

#endif // !defined(AFX_ROI_H__8438A090_F2DD_446B_99A9_8C03BAF40432__INCLUDED_)
