#ifndef UTILITY_H
#define UTILITY_H

#include "../image/image.h"
#include "../roi/roi.h"
#include <sstream>

class utility
{
	public:
		utility();
		virtual ~utility();
		static std::string intToString(int number);
        static void addGrey(image &src, image &tgt, ROI roi);
        static void greyThresh(image &src, image &tgt, ROI roi);
        static void colorThresh(image &src, image &tgt, ROI roi);
        static void scale(image &src, image &tgt, float value);
        static void adaptiveThresh(image &src, image &tgt, ROI roi);
        static void smoothing2D(image &src, image &tgt, ROI roi);
        static void smoothing1D(image &src, image &tgt, ROI roi);
        static void processImage(image &src, image &tgt, ROI roi);
};

#endif

