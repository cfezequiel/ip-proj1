#include "utility.h"
#include <assert.h>
#include <vector>
#include <cmath>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <time.h>

#define MAXLEN 256
#define CHANNELS 3

std::string utility::intToString(int number)
{
   std::stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}

/**
 * Checks if the given ROI is within an image.
 */
bool roiInImage(image img, ROI roi)
{
    int xmax;
    int ymax;
    int imgWidth = img.getNumberOfColumns();
    int imgHeight = img.getNumberOfRows();

    xmax = roi.x + roi.sx;
    ymax = roi.y + roi.sy;

    if (roi.x < 0 || roi.y < 0 || xmax > imgWidth || ymax > imgHeight)
    {
        return false;
    }

    return true;
}

/**
 * Calculates the Euclidean distance between two vectors
 */
int eucdist(int *p1, int *p2, int dim)
{
    assert(dim > 0);
    assert(p1 != NULL);
    assert(p2 != NULL);

    int d = 0;
    for (int i = 0; i < dim; i++)
    {
        d += (p1[i] - p2[i]) * (p1[i] - p2[i]);
    }
    d = (int) ceil(sqrt(d));

    return d;
}


/**
 * Compute the window mean
 */
int computeWindowMean(image &img, int hs, int vs, int i, int j)
{
    int windowMean;
    int p;
    int q;
    int n;
    int imgWidth = img.getNumberOfColumns();
    int imgHeight = img.getNumberOfRows();

    // Assume odd window size (center-reference)
    assert(hs > 0);
    assert(vs > 0);

    // Compute window mean
    windowMean = 0;
    for (int m = -hs; m <= hs; m++)
    {
        for (int n = -vs; n <= vs; n++)
        {
            p = i + m;
            q = j + n;
            if (p < 0 || p >= imgWidth || q < 0 || q >= imgHeight)
            {
                continue;
            }
            windowMean += img.getPixel(p, q);
        }
    }
    n = (hs * 2 + 1) * (vs * 2 + 1);
    windowMean = (windowMean + n) / n;

    return windowMean;
}

/*-----------------------------------------------------------------------**/
void utility::addGrey(image &src, image &tgt, ROI roi)
{
    int pixValue;

    // Run algorithm
    int weight = roi.params[0];
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.sx; j++)
        {
            pixValue = src.getPixel(i, j) + weight;
            if (pixValue > 255)
            {
                pixValue = 255;
            }
            else if (pixValue < 0)
            {
                pixValue = 0;
            }
            else 
            {
                pixValue = src.getPixel(i, j);
            }
            tgt.setPixel(i, j, pixValue);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::greyThresh(image &src, image &tgt, ROI roi)
{
    int pixValue;
    int threshold;

    // Run algorithm on ROI
    threshold = roi.params[0];
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            pixValue = src.getPixel(i, j);
            if (pixValue >= threshold)
            {
                pixValue = 255;
            }
            else
            {
                pixValue = 0;
            }
            tgt.setPixel(i, j, pixValue);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::colorThresh(image &src, image &tgt, ROI roi)
{
    int threshold;
    int color[CHANNELS];
    int pixel[CHANNELS];

    // Get threshold value for ROI
    threshold = roi.params[0];

    // Get color comparison value for ROI
    color[0] = roi.params[1];
    color[1] = roi.params[2];
    color[2] = roi.params[3];

    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            pixel[0] = src.getPixel(i, j, RED);
            pixel[1] = src.getPixel(i, j, GREEN);
            pixel[2] = src.getPixel(i, j, BLUE);

            if (eucdist(pixel, color, 3) <= threshold)
            {
                tgt.setPixel(i, j, RED, 255);
                tgt.setPixel(i, j, GREEN, 255);
                tgt.setPixel(i, j, BLUE, 255);
            }
            else
            {
                tgt.setPixel(i, j, RED, 0);
                tgt.setPixel(i, j, GREEN, 0);
                tgt.setPixel(i, j, BLUE, 0);
            }
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::adaptiveThresh(image &src, image &tgt, ROI roi)
{
    int threshold;
    int windowSize;
    int pixValue;
    int weight;
    int windowMean;

    // Get window size for ROI
    windowSize = roi.params[0];

    // Get weight value for ROI
    weight = roi.params[1];

    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            // Compute the window mean
            windowMean = computeWindowMean(src, windowSize, windowSize, i, j);

            // Set threshold
            threshold = windowMean + weight;

            // Apply threshold
            pixValue = src.getPixel(i, j);
            if (pixValue >= threshold)
            {
                pixValue = 255;
            }
            else
            {
                pixValue = 0;
            }
            tgt.setPixel(i, j, pixValue);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::smoothing2D(image &src, image &tgt, ROI roi)
{
    int windowSize;
    int windowMean;

    // Get window size for ROI
    windowSize = roi.params[0];

    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            // Compute the window mean
            windowMean = computeWindowMean(src, windowSize, windowSize, i, j);
            tgt.setPixel(i, j, windowMean);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::smoothing1D(image &src, image &tgt, ROI roi)
{
    int windowSize;
    int windowMean;

    // Get window size for ROI
    windowSize = roi.params[0];

    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            // Process horizontal window averaging
            windowMean = computeWindowMean(src, windowSize, 1, i, j);
            tgt.setPixel(i, j, windowMean);
        }
    }

    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            // Process vertical window averaging
            windowMean = computeWindowMean(tgt, 1, windowSize, i, j);
            tgt.setPixel(i, j, windowMean);
        }
    }
}

#if 0 //TODO: refactor
/*-----------------------------------------------------------------------**/
void utility::scale(image &src, image &tgt, float value)
{
    int width = src.getNumberOfRows();
    int height = src.getNumberOfColumns();

    tgt.resize(width * value, height * value);

    // Scale down
    if (value < 1)
    {
        for (int i = 0; i < tgt.getNumberOfRows(); i++)
        {
            for (int j = 0; j < tgt.getNumberOfColumns(); j++)
            {
                int sum = 0;
                int ave;
                for (int ii = 0; ii < value; ii++)
                {
                    for (int jj = 0; jj < value; jj++)
                    {
                        sum += src.getPixel(i / value + ii, j / value + jj);
                        ave = sum / value;
                    }
                }
                tgt.setPixel(i, j, ave);
            }
        }
    }
    // Scale Up
    else
    {
        for (int i = 0; i<width; i++)
        {
            for (int j = 0; j<height; j++)
            {
                int pixel = src.getPixel(i, j); 
                for (int ii = 0; ii < value; ii++)
                {
                    for (int jj = 0; jj < value; jj++)
                    {
                        tgt.setPixel(i * value + ii, j * value + jj, pixel); 
                    }
                }
            }
        }
    } // end if-else
}

#endif

/*-----------------------------------------------------------------------**/
void utility::processImage(image &src, image &tgt, ROI roi)
{
    char filter[MAXLEN];

    // Check if ROI is within image
    if (!roiInImage(src, roi))
    {
        printf("Error: ROI is not within the image.\n");
        return;
    }

    // Copy source image to target image if new
    if (tgt.isEmpty())
    {
        tgt.copyImage(src);
    }

    // Get filter
    strncpy(filter, roi.filter.c_str(), MAXLEN);

    if (strncasecmp(filter,"add",MAXLEN)==0) {

        utility::addGrey(src, tgt, roi);

    }
    else if (strncasecmp(filter,"GreyThresh",MAXLEN)==0) {
       
        // do greyscale thresholding
        utility::greyThresh(src, tgt, roi);
        
    }
    else if (strncasecmp(filter,"ColorThresh",MAXLEN)==0) {
       
        // do RGB-color thresholding
        utility::colorThresh(src, tgt, roi);
        
    }
    else if (strncasecmp(filter,"AdaptiveThresh",MAXLEN)==0) {
       
        // do adaptive thresholding
        utility::adaptiveThresh(src, tgt, roi);
        
    }
    else if (strncasecmp(filter,"smoothing2D",MAXLEN)==0) {
       
        // do 2D smoothing 
        utility::smoothing2D(src, tgt, roi);
        
    }
    else if (strncasecmp(filter,"smoothing1D",MAXLEN)==0) {

        // do 1D smoothing 
        utility::smoothing1D(src, tgt, roi);
    }
#if 0 // TODO: re-implement
    else if (strncasecmp(filter,"scale",MAXLEN)==0) {

        // do scaling
        utility::scale(src, tgt, (float) atof(argv[4]));

    }
#endif
    else
    {
        printf("Error: Unrecognized filter -> %s\n", filter);
    }
}
