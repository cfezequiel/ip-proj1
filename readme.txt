CAP5400 (Fall 2012) Assignment 1 - Image and Video Thresholding and Smoothing

Directory structure
-------------------

This software is architectured as follows

/iptools -This folder hosts the files that are compiled into a static library. 
	image - This folder hosts the files that define an image.
	utility - this folder hosts the files that students store their implemented algorithms.
	video - This folder hosts the files that define an video.
	
/lib- This folder hosts the static libraries associated with this software.

/project - This folder hosts the files that will be compiled into executables.
	/bin - This folder hosts the binary executables created in the project directory.
    /tests - This folder contains test files
        /input - Source image and video files
        /output - Processed image and video files are dumped here by the test
                  script
        /conf - Contains test parameter files
        run.sh - Test script for running all tests

/doc - Contains documentation about this project
	

Compiling the program 
----------------------

To compile this software enter the /project directory and run `make`.


Running the program 
----------------------

The program may be run from /project/bin directory.
Program syntax:

$ ./iptool [-s] SRC TGT ROI

where:
    SRC - source image/video file
    TGT - output image/video file
    ROI - ROI parameter file

Options:
    -s      Enable processing of video files

By default (i.e. without any options), the program will process image files.
Running the program without any arguments prints the syntax for the program

The ROI parameter file format specification is located in /doc.


Running the tests
-----------------

To run the tests, compile the program first.
Then, go to /project/tests and execute the run.sh script.

$ ./run.sh

A printout of each test completion should be shown.
Note that some of the video tests take some time to finish.


Report
------

## Introduction ##
The goal of this assignment is to implement basic image and video filters and investigate their behavior under different parameter settings. The filters are roughly categorized into two types: (1) binarization (thresholding), which include greyscale thresholding, color thresholding and adaptive weight thresh- olding and (2) smoothing filters, namely, one-dimensional window averaging and two-dimensional window averaging. Binarization segments each image pixel into either black (0) or white (255) color values, depending on whether the pixel value exceed a particular threshold or not. This is useful for separating distinct objects from the background in the image. Smoothing involves reducing the discrepancy in pixel values between neighborhood pixels, thus reducing data spikes. This is used to reduce noise in the image.
The filters should be applied within a specified region-of- interest (ROI) of the image. Each image can have at most two ROIs, where each ROI can have different filter parameters. For video, the filters must also work within a subsequence- of-interest (SOI), which is a set of contiguous frames within the video stream. Each ROI operates within a specific SOI. All filters should be able to process greyscale PGM files, with the exception of the color threshold filter, which must work on RGB-color PPM files.

## Conclusion ##
The test results showed the effect of different filters on both images and video. The parameters of each filter were changed in order to study the effects that the filter would have on the images. The quality of output depends the user knowledge about the sample space. Setting appropriate threshold and window size values may result in better processing quality. For image segmentation through binarization, the threshold value is key. Greyscale thresholding can be performed using a fixed or adaptive threshold value. Color thresholding can be done by choosing a reference color and a threshold value indicating the ”closeness” to that color. Smoothing can be done through local neighborhood averaging. The 1D window approach is a relatively more efficient way of doing the averaging. Applying filters to video is very similar to that of images. Object tracking in video can be done using segmentation, while smoothing can reduce noise.